<?php

use Vdbf\Propel\SchemaBuilder\Builder;
use Vdbf\Propel\SchemaBuilder\Element\Macro;
use Vdbf\Propel\SchemaBuilder\ElementStash;

require_once dirname(__DIR__) . '/vendor/autoload.php';

ElementStash::setAvailableElements(
    Builder::scanElementsPSR4(
        dirname(__DIR__) . '/src/Element/',
        'Vdbf\Propel\SchemaBuilder\Element'
    )
);

ElementStash::setAvailableElements([

    //macro that creates a x-type table
    //$database->typetable('x_types');
    new Macro([
        'element' => 'table',
        'alias' => 'typetable',
        'required' => ['name'],
        'children' => function ($table) {
            $table->integer('id')->autoIncrement(true)->primaryKey(true);
            $table->varchar('name')->primaryString(true);
            $table->behavior('sluggable', function ($b) {
                $b->parameter('column', 'name');
            });
        }
    ]),

    // macro that creates a local reference to a foreign table by assuming foreign key is a regular identifier column
    // $table->simpleLocalReference('user_id');
    // creates column[name=user_id] and foreign-key[
    new Macro([
        'element' => 'column',
        'alias' => 'simple-local-reference',
        'required' => ['name'],
        'attributes' => [
            'primaryKey' => true,
            'type' => 'BIGINT'
        ],
        'modifier' => function ($args) {
            //arg 0 = name of column, arg 1 = parent element stasher
            $args[1]->foreignKey(str_replace('_id', '', $args[0]), function ($foreign) use ($args) {
                $foreign->reference($args[0], 'id');
            });
        }
    ]),

    //macro that creates a default identifier column
    //$table->identifier();
    new Macro([
        'element' => 'column',
        'alias' => 'identifier',
        'attributes' => [
            'name' => 'id',
            'primaryKey' => true,
            'autoIncrement' => true
        ]
    ]),

    new Macro([
        'element' => 'table',
        'alias' => 'cross-table',
        'attributes' => ['isCrossRef' => true],
        'context' => ['left', 'right'],
        'children' => function ($table, $args, $self) {

            list($left, $right) = $args;

            $leftTable = $left->getAttribute('name');
            $leftForeign = $left->findChild('primaryKey', true)->getAttribute('name');
            $leftLocal = $leftTable . '_' . $leftForeign;

            $rightTable = $right->getAttribute('name');
            $rightForeign = $right->findChild('primaryKey', true)->getAttribute('name');
            $rightLocal = $rightTable . '_' . $rightForeign;

            //set table name
            $self->setAttribute('name', $leftTable . '_' . $rightTable);

            //create local pks
            $table->bigint($leftLocal)->primaryKey(true)->required(true);
            $table->bigint($rightLocal)->primaryKey(true)->required(true);

            //reference foreign pks
            $table->foreignKey($leftTable, function ($fk) use ($leftLocal, $leftForeign) {
                $fk->reference($leftLocal, $leftForeign);
            });

            $table->foreignKey($rightTable, function ($fk) use ($rightLocal, $rightForeign) {
                $fk->reference($rightLocal, $rightForeign);
            });
        }
    ]),

    /**
     * The has-one macro takes a reference to a table and extracts it to a column and a foreign key reference
     * Syntax: $table->hasOne($foreignTable);
     */
    new Macro([
        'element' => 'column',
        'alias' => 'has-one',
        'required' => ['name'],
        'attributes' => ['type' => 'BIGINT', 'required' => true],
        'modifier' => function ($args, $table, $self) {

            $foreignColumnName = $args[1]->findChild('primaryKey', true)->getAttribute('name');

            $table->foreignKey($args[1]->getAttribute('name'), function ($fk) use ($foreignColumnName, $self) {

                $fk->reference($self->getAttribute('name'), $foreignColumnName);

            });

        }
    ])

]);