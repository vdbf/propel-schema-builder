<?php

require_once 'bootstrap.php';

use Vdbf\Propel\SchemaBuilder\Builder;

$elements = function ($d) {

    $user = $d->table('user', function ($t) {

        //macro for identifier
        $t->identifier();

        //regular columns
        $t->varchar('name');
        $t->varchar('email');

        //regular columns with property overrides
        $t->varchar('password')->size(64);
        $t->varchar('remember_token')->size(64);

        //behaviors
        $t->behavior('timestampable');
    });

    $d->table('test', function ($t) use($user) {

        $t->hasOne('user_id', $user);

    });

    $userTypesTable = $d->typetable('user_type');

    $d->crossTable($user, $userTypesTable);

};

$db = Builder::database(
    'erati', 'native',
    [
        'package' => 'erati.core',
        'namespace' => 'Erati\Core\Models'
    ],
    $elements
);

//persist database, use database package or name as prefix {path}/{prefix}.schema.xml
Builder::persist($db, dirname(__DIR__));