# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Creating your schema files in a jiffy, using something more intelligent than xml.
* We're at version ALPHA

### How do I get set up? ###

* Require `vdbf/propel-schema-builder`
* Setup a bootstrap file locating all available elements (see example/bootstrap.php) and create some macro's
* Create a file to define your schema files and persist them whenever needed (see example/builder.php)

### Who do I talk to? ###

* eelkevdbos [at] gmail [dot] com