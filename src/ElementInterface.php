<?php namespace Vdbf\Propel\SchemaBuilder;

interface ElementInterface {

    public function getElementName();

    public function getElementAlias();

    public function getAttribute($key);

    public function assemble(\DOMDocument $doc);

}