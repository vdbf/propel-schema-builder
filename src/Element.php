<?php namespace Vdbf\Propel\SchemaBuilder;

abstract class Element implements ElementInterface
{

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @var array
     */
    protected $children = [];

    /**
     * @var array
     */
    protected $context = [];

    /**
     * @var null|ElementStash
     */
    protected $stash;

    /**
     * @param $args
     * @return $this
     * @throws \Exception
     */
    public function build($args)
    {
        $required = $this->getRequiredAttributes();
        $requiredCount = 0;

        if (count($args) < count($required)) {
            throw new \Exception('Element requires more attributes');
        }

        //argument parsing
        foreach ($args as $i => $arg) {

            if ($arg instanceof \Closure) {

                //callables attach elements to our current element
                $this->addChildrenViaCallable($arg, $args);

            } elseif (is_array($arg)) {

                //arrays are always a set of attributes
                $this->setAttributes($arg, true);

            } elseif (!is_object($arg)) {

                //if defined as a single argument and not a closure, it must be a required attribute
                if (isset($required[$requiredCount])) $this->setAttribute($required[$requiredCount++], $arg);

            }
        }

        return $this;
    }

    /**
     * Amend an element with new children
     * @param $callable
     * @return $this
     */
    public function amend($callable)
    {
        $args = func_get_args();
        $this->addChildrenViaCallable($callable, $args);
        return $this;
    }

    /**
     * @return string
     */
    abstract public function getElementName();

    /**
     * @return null
     */
    public function getElementAlias()
    {
        return null;
    }

    /**
     * @return ElementStash
     */
    protected function createElementStash()
    {
        if (isset($this->stash)) {
            return $this->stash;
        }
        return $this->stash = new ElementStash();
    }

    /**
     * @param $closure
     * @return $this
     */
    public function addChildrenViaCallable($callable, &$args = [])
    {
        $stash = $this->createElementStash();

        call_user_func_array($callable, [$stash, $args, $this]);

        if (count($stash->getStashed()) > 0) {

            $this->setChildren($stash->getStashed());

        }

        return $this;
    }

    /**
     * @param \DOMDocument $doc
     * @return \DOMNode
     */
    public function assemble(\DOMDocument $doc)
    {
        $element = $doc->createElement($this->getElementName());

        $node = $doc->appendChild($element);

        foreach ($this->children as $child) {

            //append our children as children of the node after assembling them
            $node->appendChild($child->assemble($doc));

        }

        foreach ($this->getAttributes() as $key => $value) {

            //copy our attributes including defaults to the node
            $node->setAttribute($key, replace_boolean($value));

        }

        return $node;
    }

    /**
     * @return array
     */
    protected function getDefaultAttributes()
    {
        return [];
    }

    /**
     * @return array
     */
    protected function getAvailableAttributes()
    {
        return [];
    }

    /**
     * @return array
     */
    protected function getRequiredAttributes()
    {
        return [];
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
        return $this;
    }

    /**
     * @param $key
     * @param string|null $defaultValue
     * @return null
     */
    public function getAttribute($key, $defaultValue = null)
    {
        return isset($this->attributes[$key]) ? $this->attributes[$key] : $defaultValue;
    }

    /**
     * @param $attributes
     * @return $this
     */
    public function setAttributes($attributes, $merge = false)
    {
        $this->attributes = $merge === false ? $attributes : array_merge($this->attributes, $attributes);
        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return array_merge($this->getDefaultAttributes(), $this->attributes);
    }

    /**
     * @return array
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param array $children
     * @param bool $merge
     */
    public function setChildren($children, $merge = false)
    {
        $this->children = $merge === false ? $children : array_merge($this->children, $children);
    }

    /**
     * Find a child ElementInterface by specifying an attribute and a value for that attribute
     * @param null $attribute
     * @param null $value
     * @return null
     */
    public function findChild($attribute = null, $value = null)
    {
        if (is_null($attribute) || is_null($value)) {
            return null;
        }

        foreach ($this->getChildren() as $child) {

            if ($child->getAttribute($attribute) === $value) {
                return $child;
            }

        }

        return null;
    }

}