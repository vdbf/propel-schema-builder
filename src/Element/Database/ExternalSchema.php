<?php namespace Vdbf\Propel\SchemaBuilder\Element\Database;

use Vdbf\Propel\SchemaBuilder\Element;

class ExternalSchema extends Element
{

    public function getElementName()
    {
        return 'external-schema';
    }

    protected function getRequiredAttributes()
    {
        return ['filename', 'referenceOnly'];
    }

}