<?php namespace Vdbf\Propel\SchemaBuilder\Element\Database;

use Vdbf\Propel\SchemaBuilder\Element;

class Table extends Element
{

    public function getElementName()
    {
        return 'table';
    }

    protected function getAvailableAttributes()
    {
        return [
            'idMethod',
            'phpName',
            'package',
            'schema',
            'namespace',
            'skipSql',
            'abstract',
            'phpNamingMethod',
            'baseClass',
            'description',
            'heavyIndexing',
            'readOnly',
            'treeMode',
            'reloadOnInsert',
            'reloadOnUpdate',
            'allowPkInsert',
            'isCrossRef'
        ];
    }

    protected function getRequiredAttributes()
    {
        return ['name'];
    }

}