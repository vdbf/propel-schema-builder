<?php namespace Vdbf\Propel\SchemaBuilder\Element\Database;

use Vdbf\Propel\SchemaBuilder\Element;

class Database extends Element
{

    /**
     * @var \DOMDocument|null
     */
    protected $document;

    public function getElementName()
    {
        return 'database';
    }

    protected function getAvailableAttributes()
    {
        return [
            'package',
            'schema',
            'namespace',
            'baseClass',
            'defaultPhpNamingMethod',
            'heavyIndexing',
            'tablePrefix'
        ];
    }

    protected function getRequiredAttributes()
    {
        return ['name', 'defaultIdMethod'];
    }

    /**
     * @return \DOMDocument|null
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param \DOMDocument|null $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

}