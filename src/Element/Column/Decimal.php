<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class Decimal extends Column
{

    public function getElementAlias()
    {
        return 'decimal';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'decimal'];
    }

}