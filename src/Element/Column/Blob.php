<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column; 

class Blob extends Column {

    public function getElementAlias()
    {
        return 'blob';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'BLOB'];
    }

}