<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class VarChar extends Column
{

    public function getElementAlias()
    {
        return 'varchar';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'VARCHAR', 'size' => 255];
    }

}