<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class ArrayColumn extends Column
{

    public function getElementAlias()
    {
        return 'array';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'ARRAY'];
    }

}