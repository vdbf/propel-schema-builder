<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

use Vdbf\Propel\SchemaBuilder\Element;

class Column extends Element
{

    public function getElementName()
    {
        return 'column';
    }

    protected function getRequiredAttributes()
    {
        return ['name'];
    }

    protected function getColumnDefaultAttributes()
    {
        return ['required' => true];
    }

    protected function getAvailableAttributes()
    {
        return [
            'phpName',
            'tableMapName',
            'primaryKey',
            'required',
            'type',
            'phpType',
            'sqlType',
            'size',
            'scale',
            'defaultValue',
            'defaultExpr',
            'valueSet',
            'autoIncrement',
            'lazyLoad',
            'description',
            'primaryString',
            'phpNamingMethod',
            'inheritance'
        ];
    }

    public function getAttributes()
    {
        return array_merge($this->getColumnDefaultAttributes(), parent::getAttributes());
    }

    public function __call($method, $arguments)
    {
        if (in_array($method, array_merge($this->getRequiredAttributes(), $this->getAvailableAttributes()))) {
            return $this->setAttribute($method, array_pop($arguments));
        }
        throw new \Exception('Method ' . $method . ' not available on class ' . get_called_class());
    }

}