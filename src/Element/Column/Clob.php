<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class Clob {

    public function getElementAlias()
    {
        return 'clob';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'CLOB'];
    }

}