<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class VarBinary extends Column
{

    public function getElementAlias()
    {
        return 'varbinary';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'VARBINARY'];
    }

}