<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class LongVarChar extends Column
{

    public function getElementAlias()
    {
        return 'longvarchar';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'LONGVARCHAR'];
    }

}