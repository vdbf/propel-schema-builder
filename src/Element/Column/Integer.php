<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class Integer extends Column
{

    public function getElementAlias()
    {
        return 'integer';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'INTEGER'];
    }

}