<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class Text extends Column
{

    public function getElementAlias()
    {
        return 'text';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'LONGVARCHAR'];
    }

}