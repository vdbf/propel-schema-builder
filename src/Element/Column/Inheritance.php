<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

use Vdbf\Propel\SchemaBuilder\Element;

class Inheritance extends Column
{

    public function getElementName()
    {
        return 'inheritance';
    }

    protected function getRequiredAttributes()
    {
        return ['key', 'class'];
    }

    protected function getAvailableAttributes()
    {
        return ['extends'];
    }

}
