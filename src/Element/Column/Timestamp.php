<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class Timestamp extends Column
{

    public function getElementAlias()
    {
        return 'timestamp';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'TIMESTAMP'];
    }

}