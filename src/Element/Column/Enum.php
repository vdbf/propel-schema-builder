<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column; 

class Enum extends Column
{

    public function getElementAlias()
    {
        return 'enum';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'ENUM'];
    }

    protected function getRequiredAttributes()
    {
        return ['name', 'valueSet'];
    }

}