<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class Binary extends Column
{

    public function getElementAlias()
    {
        return 'binary';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'BINARY'];
    }

}