<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class TinyInt extends Column
{

    public function getElementAlias()
    {
        return 'tiny-int';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'tinyint'];
    }

}