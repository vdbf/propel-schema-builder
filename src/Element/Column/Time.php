<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class Time extends Column
{

    public function getElementAlias()
    {
        return 'time';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'TIME'];
    }

}