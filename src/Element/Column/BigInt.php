<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class BigInt extends Column
{

    public function getElementAlias()
    {
        return 'bigint';
    }

    public function getDefaultAttributes()
    {
        return ['type' => 'BIGINT'];
    }

}