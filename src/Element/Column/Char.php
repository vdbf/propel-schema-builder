<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class Char extends Column
{

    public function getElementAlias()
    {
        return 'char';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'CHAR'];
    }

}