<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class Date extends Column
{

    public function getElementAlias()
    {
        return 'date';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'DATE'];
    }

}