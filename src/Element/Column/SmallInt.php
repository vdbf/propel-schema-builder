<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class SmallInt extends Column
{

    public function getElementAlias()
    {
        return 'small-int';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'smallint'];
    }

}