<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class Boolean extends Column
{

    public function getElementAlias()
    {
        return 'boolean';
    }

    public function getDefaultAttributes()
    {
        return ['type' => 'BOOLEAN'];
    }

}