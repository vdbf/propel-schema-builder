<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class Object extends Column
{

    public function getElementAlias()
    {
        return 'object';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'OBJECT'];
    }

}