<?php namespace Vdbf\Propel\SchemaBuilder\Element\Column;

class LongVarBinary extends Column
{

    public function getElementAlias()
    {
        return 'longvarbinary';
    }

    protected function getDefaultAttributes()
    {
        return ['type' => 'LONGVARBINARY'];
    }

}