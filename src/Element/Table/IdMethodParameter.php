<?php namespace Vdbf\Propel\SchemaBuilder\Element\Table;

use Vdbf\Propel\SchemaBuilder\Element;

class IdMethodParameter extends Element
{

    public function getElementName()
    {
        return 'id-method-parameter';
    }

    protected function getRequiredAttributes()
    {
        return ['value'];
    }

}