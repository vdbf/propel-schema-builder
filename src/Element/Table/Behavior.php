<?php namespace Vdbf\Propel\SchemaBuilder\Element\Table;

use Vdbf\Propel\SchemaBuilder\Element;

class Behavior extends Element
{

    public function getElementName()
    {
        return 'behavior';
    }

    protected function getRequiredAttributes()
    {
        return ['name'];
    }

}