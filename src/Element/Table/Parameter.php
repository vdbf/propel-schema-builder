<?php namespace Vdbf\Propel\SchemaBuilder\Element\Table;

use Vdbf\Propel\SchemaBuilder\Element;

class Parameter extends Element
{

    public function getElementName()
    {
        return 'parameter';
    }

    protected function getRequiredAttributes()
    {
        return ['name', 'value'];
    }

}