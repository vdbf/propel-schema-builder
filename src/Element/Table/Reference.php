<?php namespace Vdbf\Propel\SchemaBuilder\Element\Table;

use Vdbf\Propel\SchemaBuilder\Element;

class Reference extends Element
{

    public function getElementName()
    {
        return 'reference';
    }

    protected function getRequiredAttributes()
    {
        return ['local', 'foreign'];
    }

}