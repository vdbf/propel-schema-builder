<?php namespace Vdbf\Propel\SchemaBuilder\Element\Table;

use Vdbf\Propel\SchemaBuilder\Element;

class Index extends Element
{

    public function getElementName()
    {
        return 'index';
    }

    protected function getAvailableAttributes()
    {
        return ['size'];
    }

}