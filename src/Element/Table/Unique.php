<?php namespace Vdbf\Propel\SchemaBuilder\Element\Table;

use Vdbf\Propel\SchemaBuilder\Element;

class Unique extends Element
{

    public function getElementName()
    {
        return 'unique';
    }

    protected function getAvailableAttributes()
    {
        return ['name'];
    }

}