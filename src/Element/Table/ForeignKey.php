<?php namespace Vdbf\Propel\SchemaBuilder\Element\Table;

use Vdbf\Propel\SchemaBuilder\Element;

class ForeignKey extends Element
{

    public function getElementName()
    {
        return 'foreign-key';
    }

    protected function getRequiredAttributes()
    {
        return ['foreignTable'];
    }

    protected function getAvailableAttributes()
    {
        return [
            'foreignSchema',
            'name',
            'phpName',
            'refPhpName',
            'onDelete',
            'onUpdate',
            'skipSql',
            'defaultJoin'
        ];
    }

    protected function getRequiredElements()
    {
        return ['reference'];
    }

}