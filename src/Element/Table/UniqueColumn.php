<?php namespace Vdbf\Propel\SchemaBuilder\Element\Table;

use Vdbf\Propel\SchemaBuilder\Element;

class UniqueColumn extends Element
{

    public function getElementName()
    {
        return 'unique-column';
    }

    protected function getRequiredAttributes()
    {
        return ['name'];
    }

    protected function getAvailableAttributes()
    {
        return ['size'];
    }

}