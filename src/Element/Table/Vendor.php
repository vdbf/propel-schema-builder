<?php namespace Vdbf\Propel\SchemaBuilder\Element\Table;

use Vdbf\Propel\SchemaBuilder\Element;

class Vendor extends Element
{

    public function getElementName()
    {
        return 'vendor';
    }

    protected function getRequiredAttributes()
    {
        return ['type'];
    }

}