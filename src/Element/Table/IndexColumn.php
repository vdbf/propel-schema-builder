<?php namespace Vdbf\Propel\SchemaBuilder\Element\Table;

use Vdbf\Propel\SchemaBuilder\Element;

class IndexColumn extends Element
{

    public function getElementName()
    {
        return 'index-column';
    }

    protected function getRequiredAttributes()
    {
        return ['name'];
    }

}