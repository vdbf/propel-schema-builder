<?php namespace Vdbf\Propel\SchemaBuilder\Element;

use Vdbf\Propel\SchemaBuilder\Element;
use Vdbf\Propel\SchemaBuilder\ElementStash;

class Macro extends Element
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $alias;

    /**
     * @var mixed
     */
    protected $callable;

    /**
     * @var array
     */
    protected $required;

    /**
     * @var null
     */
    protected $modifier;

    /**
     * @param array $attrs
     */
    public function __construct($attrs = [])
    {
        $this->name = self::getArrayElementByKey($attrs, 'element');
        $this->alias = self::getArrayElementByKey($attrs, 'alias');
        $this->callable = self::getArrayElementByKey($attrs, 'children');
        $this->attributes = self::getArrayElementByKey($attrs, 'attributes', []);
        $this->required = self::getArrayElementByKey($attrs, 'required', []);
        $this->modifier = self::getArrayElementByKey($attrs, 'modifier', null);
    }

    /**
     * Build the Macro element by analysing the arguments supplied during the call to the element alias
     * @param $args
     * @param ElementStash $creator
     * @return $this
     * @throws \Exception
     */
    public function build($args, ElementStash $creator = null)
    {
        if (is_callable($this->callable)) {

            //if a callable is supplied (template for macro), execute it to add children to the element
            $this->addChildrenViaCallable($this->callable, $args);

        }

        parent::build($args);

        if (is_callable($this->modifier)) {

            call_user_func_array(
                $this->modifier,
                [$args, $creator, $this]
            );

        }

        return $this;
    }

    /**
     * Get the element name, resulting in a <{$name} /> element when assembled
     * @return null|string
     */
    public function getElementName()
    {
        return $this->name;
    }

    /**
     * Get the element alias (method name callable from any stash)
     * @return null|string
     */
    public function getElementAlias()
    {
        return $this->alias;
    }

    /**
     * Get required attributes for the Macro
     * @return array
     */
    public function getRequiredAttributes()
    {
        return $this->required ?: [];
    }

    /**
     * Retrieve an element from an array by key, enabling a default value if the key is missing
     * @param $arrayAccess
     * @param $key
     * @param null $default
     * @return null
     */
    static function getArrayElementByKey($arrayAccess, $key, $default = null)
    {
        if (isset($arrayAccess[$key])) {
            return $arrayAccess[$key];
        }
        return $default;
    }

    /**
     * Subsequent calls to the created macro element will set attribute properties
     * @param $method
     * @param $arguments
     * @return $this
     */
    public function __call($method, $arguments)
    {
        return $this->setAttribute($method, array_pop($arguments));
    }

}
