<?php namespace Vdbf\Propel\SchemaBuilder;

function replace_dashes($name)
{
    $chunks = explode('-', $name);
    return array_shift($chunks) . implode('', array_map('ucfirst', $chunks));
}

function replace_boolean($value)
{
    if ($value === true) {
        return 'true';
    } elseif ($value === false) {
        return 'false';
    }
    return $value;
}