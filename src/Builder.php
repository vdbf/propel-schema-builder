<?php namespace Vdbf\Propel\SchemaBuilder;

use DOMDocument;
use ReflectionClass;
use Vdbf\Propel\SchemaBuilder\Element\Database\Database;

class Builder
{

    /**
     * @var string
     */
    public static $elementStashClass = 'Vdbf\Propel\SchemaBuilder\ElementStash';

    /**
     * @var array
     */
    public $databases = [];

    /**
     * Constructor for a new builder object
     *
     * @param array $arguments
     */
    private function __construct($arguments)
    {
        $stash = $this->createElementStash($arguments);

        foreach ($stash->getStashed() as $database) {

            /** @var Element\Database\Database $database */
            $document = $this->createDomDocument();
            $document->formatOutput = true;
            $database->assemble($document);
            $database->setDocument($document);

            $this->databases[] = $database;

        }
    }
    
    /**
     * Get a table reference from an external schema, watch out for recursion includes
     * @param $path
     * @param $tableIdentifier
     * @param string $attribute
     * @return null|Database
     */
    public static function getTableReference($path, $tableIdentifier, $attribute = 'name')
    {
        /** @var Database $database */
        $database = include $path;

        if ($database instanceof Database) {
            return $database->findChild($attribute, $tableIdentifier);
        }
    }

    /**
     * Create a new element stash to factorize and stash various schema elements
     *
     * @param array $arguments
     * @return ElementStash
     */
    protected function createElementStash($arguments)
    {
        $stash = new self::$elementStashClass();
        call_user_func_array([$stash, 'database'], $arguments);

        return $stash;
    }

    /**
     * Wrapper for DOMDocument creation
     *
     * @return DOMDocument
     */
    protected function createDomDocument()
    {
        return new DOMDocument('1.0', 'UTF-8');
    }

    /**
     * Public constructor for a new database builder
     *
     * @return Element\Database\Database
     */
    public static function database()
    {
        $builder = new self(func_get_args());

        return current($builder->databases);
    }

    /**
     * Scan for ElementInterface implementations in a PSR-4 esque way by coupling a base path to a base namespace,
     * and proceed by recursively walking the folder structure
     *
     * @param $path
     * @param $namespace
     * @return array
     */
    public static function scanElementsPSR4($path, $namespace)
    {
        $elements = [];

        $contents = scandir($path);

        foreach ($contents as $entry) {

            //scan for php files and add them to the elements list when they implement the correct i-face
            if (strpos($entry, '.php') !== false) {

                $reflection = new ReflectionClass($namespace . '\\' . substr($entry, 0, -4));

                if ($reflection->implementsInterface('Vdbf\Propel\SchemaBuilder\ElementInterface') && $reflection->getName() != 'Vdbf\Propel\SchemaBuilder\Element\Macro') {

                    $elements[] = $reflection->newInstance();

                }

            } elseif (strpos($entry, '.') === false) {

                //if not a file but directory, recurse our search for php files
                $elements = array_merge(
                    self::scanElementsPSR4($path . '/' . $entry, $namespace . '\\' . $entry),
                    $elements
                );

            }
        }

        return $elements;
    }

    /**
     * @param Database $db
     * @param null $path
     * @author Eelke van den Bos <eelkevdbos@gmail.com>
     * @return null|string
     */
    public static function persist(Database $db, $path = null)
    {
        $result = file_put_contents(
            $file = $path . '/' . $db->getAttribute('package', $db->getAttribute('name')) . '.schema.xml',
            $db->getDocument()->saveXml()
        );

        return $result ? $file : null;
    }

}
