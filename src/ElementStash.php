<?php namespace Vdbf\Propel\SchemaBuilder;

class ElementStash
{

    /**
     * @var array
     */
    protected $stashed;

    /**
     * @var array
     */
    protected static $availableElements = [];

    /**
     * @param $method
     * @param array $arguments
     * @return object
     * @throws \Exception
     */
    public function __call($method, $arguments = [])
    {
        if (isset(self::$availableElements[$method])) {

            $instance = clone self::$availableElements[$method];
            $instance->build($arguments, $this);

            $this->stashed[] = $instance;
            return $instance;
        }

        throw new \Exception('Method ' . $method . ' not available');
    }

    /**
     * List of ElementInterface instances with element name or alias as key
     * @return array
     */
    public static function getAvailableElements()
    {
        return self::$availableElements;
    }

    /**
     * Setter for list of ElementInterface instances
     * @param $elements
     */
    public static function setAvailableElements($elements)
    {
        foreach ($elements as $element) {
            /** @var ElementInterface $element */
            self::$availableElements[replace_dashes($element->getElementAlias() ?: $element->getElementName())] = $element;
        }
    }

    /**
     * @return array
     */
    public function getStashed()
    {
        return $this->stashed;
    }

    /**
     * @param array $stashed
     */
    public function setStashed($stashed)
    {
        $this->stashed = $stashed;
    }

}
